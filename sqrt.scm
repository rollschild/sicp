(define (sqrt x) (sqrt-iteration x 1.0))

(define (sqrt-iteration x guess)
  (if (good-enough x guess) 
     guess
     (sqrt-iteration x (improve x guess))))

(define (improve x guess) 
  (average guess (/ x guess)))

(define (average x y) (/ (+ x y) 2))

(define (good-enough x guess) 
  (< (abs (- (square guess) x)) 0.001))

(define (square a) (* a a))

(define (abs m) (if (< m 0) (- m) m))
